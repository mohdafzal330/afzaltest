<?php
    require_once('admin/request.php');
?>
<!DOCTYPE html>
<html>
<head>
    <title>Admin Request Managment Panel</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <script type="text/javascript" src="admin/status.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row" style="background-color: #1c1919;color: white;">
            <div class="col-lg-4">
               <center><h3>Admin Panel</h3></center>
            </div>
            <div class="col-lg-5">
                
            </div>
            <div class="col-lg-3">
                <h4><a href="admin/logout.php">Logout</a></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2" style="background-color: #d4eaf6;height: 600px;color: black">
                <br><hr>
                <a href="admin.php">Dashboard</a>
            </div>
            <div class="col-lg-10">
                <h3 style="color: blue">Dashboard</h3><hr>
                <h4 style="color: green">All non approved request are listed below</h4><br>
                <div>
                    <table class="table ">
                        
                        <tr style="color: white;background-color: red;">
                            <td>ID</td><td>Name</td><td>Email</td><td>Details</td><td>Status</td><td>CLick here</td>
                            </form>
                        </tr>
                        <?php
                            for($i=1;$i<=$row_count;$i++){
                                $show_data = mysqli_fetch_array($result);
                                if($show_data['approved']==0 && $show_data['spam_detection']==0){
                            ?>
                        <tr>
                            
                            <form action="admin/update_status.php" method="post">
                            <input hidden type="number" name="id" id="id" value="<?php echo $show_data['id']?>">
                            <td><?php echo $show_data['id']?></td>
                            <td><?php echo $show_data['name']?></td>
                            <td><?php echo $show_data['email']?></td>
                            <td><a href="admin/show.php?id=<?php echo $show_data['id']?>" target="_blank">Show Details</a></td>
                            <td>
                                Approved<input type="radio" name="status" required value="approved">
                                Rejected<input type="radio" name="status" required value="spam_detection">
                            </td>
                            <td>
                                <input type="submit" class="btn btn-sm btn-info" name="submit" value="Done">
                            </td>
                            </form>
                         </tr>
                      <?php } } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

</body>
</html>