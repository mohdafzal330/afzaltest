<?php
require_once 'session_check.php'
?>

<!DOCTYPE html>
<html>
<head>
	<script src="post_request.js"></script>
	<script src="jquery-3.2.1.min.js"></script>
	<title></title>
</head>
<body>
	<!-- <form class="form" action="demo_insert_controller.php" method="post" id = "userForm">
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name" id = "name"/>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email"/>
        </div>
        <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" name="phone"/>
        </div>
        <div class="form-group">
        	<input type="submit" name="submit">
        </div>
    </form> -->
    <form class="form" onsubmit="event.preventDefault(); ajaxSubmitForm(this);" action="demo_insert_controller.php" name="userForm" id="userForm">
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="name"/>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email"/>
        </div>
        <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" name="phone"/>
        </div>
        <div class="form-group">
        	<input type="submit" name="submit">
        </div>
    </form>
</body>
</html>