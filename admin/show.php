<?php
    require_once('get.php');

?>
<!DOCTYPE html>
<html>
<head>
    <title>Admin Request Managment Panel</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <script type="text/javascript" src="../admin/status.js"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row" style="background-color: #1c1919;color: white;">
            <div class="col-lg-4">
               <center><h3>Admin Panel</h3></center>
            </div>
            <div class="col-lg-5">
                
            </div>
            <div class="col-lg-3">
                <h4><a href="logout.php">Logout</a></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2" style="background-color: #d4eaf6;height: 600px;color: black">
                <br><hr>
                <a href="../admin.php">Dashboard</a>
            </div>
            <div class="col-lg-10">
                <h3 style="color: blue">Details of <?php echo $print['name']?></h3><hr>
                <div>
                    <fieldset>
                    <table class="table ">
                        <tr>
                            <td><label>Name : </label></td><td><?php echo $print['name']?></td>
                        </tr>
                        <tr>
                            <td><label>Email : </label></td><td><?php echo $print['email']?></td>
                        </tr>
                        <tr>
                            <td><label>Internship Location : </label></td><td><?php echo $print['loc']?></td>
                        </tr>
                         <tr>
                            <td><label>Category : </label></td><td><?php echo $print['category']?></td>
                        </tr>
                        <tr>
                            <td><label>Orgnization Name : </label></td><td><?php echo $print['orgname']?></td>
                        </tr>
                         <tr>
                            <td><label>Internship Source : </label></td><td><?php echo $print['intsource']?></td>
                        </tr>
                         <tr>
                            <td><label>Website Name (If Applicable): </label></td><td><?php echo $print['webname']?></td>
                        </tr>
                        <tr>
                            <td><label>Selection Procedure : </label></td><td><?php echo $print['slprocedure']?></td>
                        </tr>
                        <tr>
                            <td><label>Interview Question : </label></td><td><?php echo $print['intque']?></td>
                        </tr>
                        <tr>
                            <td><label>Internship Preparation : </label></td><td><?php echo $print['preparation']?></td>
                        </tr>
                        <tr>
                            <td><label>Internship Experience : </label></td><td><?php echo $print['intexp']?></td>
                        </tr>
                       </table>
                    </fieldset>
                </div>
            </div>       
        </div>
    </div>

</body>
</html>